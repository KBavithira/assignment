package bcas.ap.vehicle;

public class FuelVechile extends RentedVechile {
	private double nbKms;
	
	public FuelVechile(double basefees, double nbKms) {
		super(basefees);
		this.nbKms = nbKms;
	}

	public double getMileageFees() {
		if(nbKms < 100) {
		return 0.2 * nbKms;
	}
		else if (nbKms <= 400 || nbKms >= 100) {
			return 0.3 * nbKms;
		}else {
			return 0.3 * 400 + (nbKms - 400) * 0.5;
		}

	
	
	}
}
