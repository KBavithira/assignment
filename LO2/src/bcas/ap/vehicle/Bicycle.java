package bcas.ap.vehicle;

public class Bicycle extends RentedVechile {
	private String Name;
	private int nbDays;
	

	public Bicycle(double basefees, int nbDays) {
		super(basefees);
		this.nbDays = nbDays;
	}
	
	public double getCost() {
		if(nbDays < 1) {
			return basefees * nbDays;
		} else {
			return basefees;
		}
	}

	public String getName() {
		return Name;
	}
}
