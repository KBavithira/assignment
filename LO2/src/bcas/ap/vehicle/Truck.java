package bcas.ap.vehicle;

public class Truck extends FuelVechile {
	private String Name;
	private int capacity;
	
	
	public Truck(double basefees, double nbKms, int capacity) {
		super(basefees, nbKms);
		this.capacity = capacity;
	}

	public double getCost() {
		if (capacity < 16.7 ) {
			return basefees*capacity;
		}
		else
			return capacity;
	}

	public String getName() {
		return Name;
	}

}

