package bcas.OnlineShopping.Data;

import bcas.Online.shopping.ShoppingCart;
import bcas.Online.shopping.Web_User;
import java.util.Set;
import bcas.Online.shopping.LineItem;

public interface CartData {
	
	public ShoppingCart create(ShoppingCart c);
	public void add(ShoppingCart c);
	public ShoppingCart read(ShoppingCart c);	
	public Set<LineItem> read(ShoppingCart c, Web_User Id);
	public void update(ShoppingCart c);
	public void delete(ShoppingCart c);
	
	

}
