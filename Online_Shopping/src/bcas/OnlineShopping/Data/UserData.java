package bcas.OnlineShopping.Data;

import bcas.Online.shopping.New_User;
import bcas.Online.shopping.Web_User;

public interface UserData {
	public int New(New_User u);
	public Web_User read(Web_User u);
    public int Active(Web_User u);
    public int Blocked(Web_User u);
    public int Banned(Web_User u);
    public boolean validateUser(New_User u,String OTP);
	public New_User read(New_User u);
 

}
