package bcas.OnlineShopping.Data;

import java.util.List;

import bcas.Online.shopping.Category;
import bcas.Online.shopping.Product;

public interface productData {
    public void create(Product c);
    public Product read(Product c);
    public List<Product> read(Category c);
    public List<Product> read();
    public void update(Product c);
    public void delete(Product c);
}
