package bcas.OnlineShopping.Data;

import java.util.List;

import bcas.Online.shopping.Category;

public interface CategoryData {
    public void create(Category c);
    public Category read(Category c);
    public List<Category> read();
    public void update(Category c);
    public void delete(Category c);
}