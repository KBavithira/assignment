package bcas.OnlineShopping.Data_Implements;

import java.sql.ResultSet;

import bcas.Online.shopping.New_User;
import bcas.Online.shopping.Web_User;
import bcas.OnlineShopping.Data.UserData;
import bcas.OnlineShopping.db.DataSource;

public class Web_UserDataImpl implements UserData{
	DataSource ds =new DataSource();

	@Override
	public int New(New_User u) {
		 int i=0;
	        try{
	            if(isNewUser(u)){
	        ds.setCon();
	        ds.setSt("insert into Users(firstName,lastName,mailId,phoneNumber,password,status) values(?,?,?,?,?,?)");
	        ds.getSt().setString(1,u.getFirstName());
	        ds.getSt().setString(2,u.getLastName());
	        ds.getSt().setString(3,u.getMailId());
	        ds.getSt().setString(4,u.getPhoneNumber());
	        ds.getSt().setString(5,u.getPassword());
	        ds.getSt().setString(6,u.getStatus());
	        i=ds.getSt().executeUpdate();
	        ds.getCon().commit();
	        ds.getCon().close();
	            }
	        }catch(Exception e){
	            System.err.println(e);
	        }
	        return i; 
	}

	private boolean isNewUser(New_User u) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public New_User read(New_User u) {
		try{
            ds.setCon();
            ds.setSt("select * from Users where mailId=?");
            ds.getSt().setString(1, u.getMailId());
            ResultSet rs = ds.getSt().executeQuery();
            if(rs.next()){
                if(rs.getString("password").equals(u.getPassword())){
                    u.setFirstName(rs.getString("firstName"));
                    u.setLastName(rs.getString("lastName"));
                    u.setPhoneNumber(rs.getString("phoneNumber"));
                    u.setId(rs.getInt("Id"));
                    u.setStatus(rs.getString("status"));
                    u.setOTP(rs.getString("OTP"));
                }
            }
        }catch(Exception e){
            System.err.println(e);
        }
        return u;
	}

	@Override
	public int Active(Web_User u) {
		return 0;
	}

	@Override
	public int Blocked(Web_User u) {
		return 0;
	}

	@Override
	public int Banned(Web_User u) {
		return 0;
	}

	@Override
	public boolean validateUser(New_User u, String OTP) {
		try{
            ds.setCon();
            ds.setSt("select * from Users where mailId=?");
            ds.getSt().setString(1, u.getMailId());
            ResultSet rs = ds.getSt().executeQuery();
            if(rs.next()){
                return false;
            }
        }catch(Exception e){
            System.err.println(e);
        }
        return true;
	}

	@Override
	public Web_User read(Web_User u) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
