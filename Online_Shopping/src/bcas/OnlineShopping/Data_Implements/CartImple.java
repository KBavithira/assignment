package bcas.OnlineShopping.Data_Implements;

import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;

import bcas.Online.shopping.LineItem;
import bcas.Online.shopping.Product;
import bcas.Online.shopping.ShoppingCart;
import bcas.Online.shopping.Web_User;
import bcas.OnlineShopping.Data.CartData;
import bcas.OnlineShopping.Data.productData;
import bcas.OnlineShopping.db.DataSource;

public class CartImple implements CartData{
	DataSource ds = new DataSource();
	

	@Override
	public ShoppingCart create(ShoppingCart c) {
		try{
	        ds.setCon();
	        ds.setSt("insert into Cart(Userid) values (?)");
	        ds.getSt().setString(1,c.getUser().getId());
	        ds.getSt().executeUpdate();
	        ds.getCon().commit();
	        ds.getCon().close();
	        }catch(Exception e){
	            System.out.println(e);
	        }
	        c=read(c);
	        return c;
	}

	@Override
	public void add(ShoppingCart c) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] arg){
        CartData cd=new CartImple();
        
    }

	@Override
	public ShoppingCart read(ShoppingCart c) {
		 try{
	            ds.setCon();
	            ds.setSt("select * from Cart where userId= ?");
	            ds.getSt().setString(1, c.getUser().getId());
	            ResultSet rs= ds.getSt().executeQuery();
	            if(rs.next()){
	                    c.setId(rs.getString("Id"));
	            }
	        }catch(Exception e){}
	        return c;
	}

	@Override
	public Set<LineItem> read(ShoppingCart c, Web_User Id) {
		 Set<LineItem> cartItems = new HashSet<>();
         try{
        ds.setCon();
        ds.setSt("select * from CartItem where cartId= ?");
        ds.getSt().setString(1, ShoppingCart.getId());
        ResultSet rs= ds.getSt().executeQuery();
        while(rs.next()){
        	LineItem ci=new LineItem();
            ci.setId(rs.getString("Id"));
            ShoppingCart c=new ShoppingCart();
            c.setId(rs.getString("cartId"));
            ci.setCart(c);
            Product p=new Product(rs.getString("productId"));
            productData pd=new ProductDAOImpl();
            ci.setProduct(pd.read(p));
            ci.setQuantity(rs.getInt("quantity"));
            cartItems.add(ci);
        }
    }catch(Exception e){}
    return cartItems;
	}

	@Override
	public void update(ShoppingCart c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(ShoppingCart c) {
		// TODO Auto-generated method stub
		
	}

}
