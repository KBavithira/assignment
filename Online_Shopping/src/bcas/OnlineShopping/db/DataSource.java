package bcas.OnlineShopping.db;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DataSource {
	private Connection con;
    private PreparedStatement st;

    public Connection getCon() {
        return con;
    }

    public void setCon() {
        try {
            this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/onlineshopping\",\"root\",\"root");
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }

    public PreparedStatement getSt() {
        return st;
    }

    public void setSt(String query) {
        try {
            this.st = con.prepareCall(query);
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }

}
