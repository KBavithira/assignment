package bcas.Online.shopping;

import java.util.HashSet;
import java.util.Set;

public class ShoppingCart {
	private String Id;
	private Web_User user;
	private Set<LineItem> LineItem=new HashSet<>();
	
	public ShoppingCart() {	
	}
	
	public ShoppingCart (Web_User user) {
		this.setUser(user);
	}

	public  String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public Web_User getUser() {
		return user;
	}

	public void setUser(Web_User user) {
		this.user = user;
	}
	
	public Set<LineItem> getLineItem(){
		return LineItem;
	}
	
	public void SetLineItem(Set<LineItem> LineItem) {
		this.LineItem = LineItem;
	}

}
