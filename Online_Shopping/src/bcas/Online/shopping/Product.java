package bcas.Online.shopping;

	public class Product{
	    private String product_Id;
	    private String Product_Name;
	    private String supplier;
	   

	    public Product() {
	    }

	    public Product(String product_Id, String Product_Name, String supplier) {
	        this.setProduct_Id(product_Id);
	        this.setProduct_Name(Product_Name);
	        this.setSupplier(supplier);
	      }

		public String getProduct_Id() {
			return product_Id;
		}

		public void setProduct_Id(String product_Id) {
			this.product_Id = product_Id;
		}

		public String getProduct_Name() {
			return Product_Name;
		}

		public void setProduct_Name(String product_Name) {
			Product_Name = product_Name;
		}

		public String getSupplier() {
			return supplier;
		}

		public void setSupplier(String supplier) {
			this.supplier = supplier;
		}

}
