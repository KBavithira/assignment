package bcas.Online.shopping;

public class Customer {
	private String Id;
    private Address adress;
    private int phone;
    private String email;

    public Customer() {
    }

    public Customer(String Id, Address address, int phone, String email) {
       // this.setId(Id);
    	this.setId(Id);
    	this.setAdress(address);
    	this.setPhone(phone);
    	this.setEmail(email);
      }

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public Address getAdress() {
		return adress;
	}

	public void setAdress(Address adress) {
		this.adress = adress;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


}
