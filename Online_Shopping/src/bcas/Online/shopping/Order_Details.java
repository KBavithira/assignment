package bcas.Online.shopping;

public class Order_Details {
	   private String serialNumber;
	   private Date odered;
	   private Date shipped;
	   private Address ship_to;
	   private OrderStatus status;
	   private Real total;
	   
	   public Order_Details(String serialNumber, Date odered, Date shipped, Address ship_to, OrderStatus status, Real total ) {
		   this.setSerialNumber(serialNumber);
		   this.setOdered(odered);
		   this.setShipped(shipped);
		   this.setShip_to(ship_to);
		   this.setStatus(status);
		   this.setTotal(total);
	   }
	   
	   public Order_Details(String serialNumber) {
		   this.serialNumber = serialNumber;
	   }

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Date getOdered() {
		return odered;
	}

	public void setOdered(Date odered) {
		this.odered = odered;
	}

	public Date getShipped() {
		return shipped;
	}

	public void setShipped(Date shipped) {
		this.shipped = shipped;
	}

	public Address getShip_to() {
		return ship_to;
	}

	public void setShip_to(Address ship_to) {
		this.ship_to = ship_to;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public Real getTotal() {
		return total;
	}

	public void setTotal(Real total) {
		this.total = total;
	}
	   
	   

}
