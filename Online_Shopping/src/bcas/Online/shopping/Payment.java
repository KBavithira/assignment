package bcas.Online.shopping;

public class Payment {
	  private String Id;
	  private Date paid;
	  private Real total;
	  private String details;
	  
	  public Payment(String Id, Date paid, Real total, String details) {
			this.setId(Id);
			this.setPaid(paid);
			this.setTotal(total);
			this.setDetails(details);
	  }
	  
	  
	public String getId() {
		return Id;
	}
	
	public void setId(String id) {
		Id = id;
	}
	
	public Date getPaid() {
		return paid;
	}
	
	public void setPaid(Date paid) {
		this.paid = paid;
	}
	
	public Real getTotal() {
		return total;
	}
	
	public void setTotal(Real total) {
		this.total = total;
	}
	
	public String getDetails() {
		return details;
	}
	
	public void setDetails(String details) {
		this.details = details;
	}
	    

}
