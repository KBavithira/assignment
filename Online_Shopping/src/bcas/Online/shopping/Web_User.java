package bcas.Online.shopping;

public class Web_User {
	private String Id;
	private String password;
	private Userstate state;
	
	public Web_User(String Id, String password, Userstate state ) {
		this.setId(Id);
		this.setPassword(password);
		this.setState(state);
	}
	
	public Web_User(String Id, String password ) {
		this.setId(Id);
		this.setPassword(password);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public Userstate getState() {
		return state;
	}

	public void setState(Userstate state) {
		this.state = state;
	}
	
	

}
