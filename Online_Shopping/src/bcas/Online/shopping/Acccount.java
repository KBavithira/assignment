package bcas.Online.shopping;

public class Acccount {
	private String Id;
	private Address billing_address;
	private Boolean is_closed;
	private Date open;
	private Date closed;
	
	public Acccount(String Id, Address billing_address, Boolean is_closed, Date open, Date closed) {
		this.setId(Id);
		this.setBilling_address(billing_address);
		this.setIs_closed(is_closed);
		this.setOpen(open);
		this.setClosed(closed);
		
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public Address getBilling_address() {
		return billing_address;
	}

	public void setBilling_address(Address billing_address) {
		this.billing_address = billing_address;
	}

	public Boolean getIs_closed() {
		return is_closed;
	}

	public void setIs_closed(Boolean is_closed) {
		this.is_closed = is_closed;
	}

	public Date getOpen() {
		return open;
	}

	public void setOpen(Date open) {
		this.open = open;
	}

	public Date getClosed() {
		return closed;
	}

	public void setClosed(Date closed) {
		this.closed = closed;
	}

}
